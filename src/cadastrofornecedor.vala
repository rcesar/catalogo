using Gtk,Sqlite;

//extern Database db;

class CadFornecedor: Bin {
    TreeView lista = new TreeView();
    TreeView lista_endereco;
    TreeView lista_telefone;
    TreeView lista_internet;
    Entry nome = new Entry();
    Entry codigo = new Entry();
    Entry pad = new Entry();
    public CadFornecedor()
    {
      lista.set_size_request(-1,500);
      codigo.set_text("");
      //=============
      var wbase = new Box(Orientation.HORIZONTAL,0);
      add(wbase);
      //lista de fornecedores e caixa de buscas
      var bxforn = new Box(Orientation.VERTICAL,0);
      var bxforn1 = new Box(Orientation.HORIZONTAL,0);
      var bt1 = new ToolButton(new Image.from_icon_name("gtk-find",IconSize.LARGE_TOOLBAR),"");
      bxforn1.pack_start(pad,true,true);
      bxforn1.pack_end(bt1,false,false);
      var fr = new Frame("");
      var scw = new ScrolledWindow(null,null);
      scw.set_policy(PolicyType.AUTOMATIC,PolicyType.AUTOMATIC);
      fr.add(scw);
      scw.add(lista);
      var fr2 = new Frame("Busca:");
      fr2.add(bxforn1);
      //barra de ferramentas apagar fornecedor e novo fornecedor
      var tb_f = new Toolbar();
      var btn = new ToolButton(new Image.from_icon_name("gtk-new",IconSize.LARGE_TOOLBAR),"");tb_f.add(btn);
      btn.clicked.connect(this.novo_cadastro);
      var btn1 = new ToolButton(new Image.from_icon_name("gtk-delete",IconSize.LARGE_TOOLBAR),"");tb_f.add(btn1);
      btn1.clicked.connect(apaga_cadastro);
      //
      bxforn.pack_start(fr,true,true);
      bxforn.pack_start(fr2,false,true);
      bxforn.pack_end(tb_f,false,true);
      wbase.pack_start(bxforn,false,true);
      //---
      var bxcad = new Box(Orientation.VERTICAL,0);
      //campo nome e botão para editar
      var frnome = new Frame("Nome:");
      var bnome = new Box(Orientation.HORIZONTAL,0);
      var btedit= new ToolButton(new Image.from_icon_name("gtk-edit",IconSize.SMALL_TOOLBAR),"");
      btedit.clicked.connect(edita_nome);
      bnome.pack_start(nome,true,true);
      bnome.pack_end(btedit,false,false);
      frnome.add(bnome);
      nome.editable=false;
      //
      var dkp = new Notebook();
      var endereco = new TabEndereco("Endereço");
      endereco.add_button.clicked.connect(this.adiciona_endereco);
      endereco.del_button.clicked.connect(this.apaga_endereco);
      lista_endereco=endereco.lista;
      lista_endereco.row_activated.connect(edita_endereco);
      var telefone = new TabEndereco("Telefone");
      telefone.add_button.clicked.connect(adiciona_telefone);
      telefone.del_button.clicked.connect(apaga_telefone);
      lista_telefone=telefone.lista;
      lista_telefone.row_activated.connect(edita_telefone);
      var internet = new TabEndereco("Internet");
      lista_internet=internet.lista;
      internet.add_button.clicked.connect(adiciona_internet);
      internet.del_button.clicked.connect(apaga_internet);
      lista_internet.row_activated.connect(edita_internet);
      //---
      dkp.append_page(endereco, endereco.create_label());
      dkp.append_page(telefone, telefone.create_label());
      dkp.append_page(internet, internet.create_label());
      bxcad.pack_start(frnome,false,true);
      bxcad.pack_end(dkp,true,true);
      wbase.pack_end(bxcad,true,true);
      lista.row_activated.connect(abre_cadastro);
      lista.insert_column_with_attributes(-1,"Código",new Gtk.CellRendererText(),"text",0);
      lista.insert_column_with_attributes(-1,"Fornecedores cadastrados:",new Gtk.CellRendererText(),"text",1);
      lista.get_column(0).set_visible(false);
      atualizar();
      }

    private void apaga_cadastro()
    {
        var md=this.lista.get_model();
        var it = TreeIter();
        var resultado = this.lista.get_selection().get_selected(out md,out it);
        if (resultado) {
            GLib.Value codigo;
            GLib.Value nome;
            md.get_value(it,0,out codigo);
            md.get_value(it,1,out nome);
            var dlg = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.YES_NO,"Deseja apagar os dados de "+(string)nome+"?");
            if (dlg.run()==ResponseType.YES)
            {
                int ret=db.exec("DELETE FROM fornecedor WHERE codigo='"+(string)codigo+"'",null);
                if (ret!=Sqlite.OK) {stderr.printf(db.errmsg());}
                ret=db.exec("DELETE FROM telefone WHERE codigo_fornecedor='"+(string)codigo+"'",null);
                if (ret!=Sqlite.OK) {stderr.printf(db.errmsg());}
                ret=db.exec("DELETE FROM endereco WHERE codigo_fornecedor='"+(string)codigo+"'",null);
                if (ret!=Sqlite.OK) {stderr.printf(db.errmsg());}
            }
            dlg.destroy();
            atualizar();
        }
    }

  private void abre_cadastro()
  {
    var md=this.lista.get_model();
    var it = TreeIter();
    var resultado = this.lista.get_selection().get_selected(out md,out it);
    if (resultado) {
        GLib.Value codigo;
        GLib.Value nome;
        md.get_value(it,0,out codigo);
        md.get_value(it,1,out nome);
        this.codigo.set_text((string)codigo);
        this.nome.set_text((string)nome);
        atualiza_lista_enderecos();
        atualiza_lista_telefones();
        atualiza_lista_internet();
    }
  }

  private void edita_nome()
  {
    var dlg = new DLGNome(this.nome.get_text());
    if (dlg.run()==1)
    {
        Statement stm;
        string query = "UPDATE fornecedor SET nome=$NOME WHERE codigo=$CODIGO";
        db.prepare_v2(query,query.length,out stm);
        stm.bind_text(1,dlg.nome.get_text());
        stm.bind_text(2,this.codigo.get_text());
        if (stm.step()!=Sqlite.DONE)
        {
            var dle = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
            dle.run();dle.destroy();
            return;
        }
        this.nome.set_text(dlg.nome.get_text());
    }
    dlg.destroy();
    atualizar();
}

    private void novo_cadastro()
    {
        var dlg = new DLGNome();
        if (dlg.run()==1)
        {
            string nome = dlg.nome.get_text();
            Statement stm;
            string query = "SELECT count(nome) FROM fornecedor WHERE nome LIKE $NOME";
            db.prepare_v2(query,query.length,out stm);
            stm.bind_text(1,nome);
            stm.step();
            if (stm.column_int(0)>0)
            {
                var dl = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Já existe fornecedor com este nome");
                dl.run();dl.destroy();
                return;
            } else {
                Statement stinsert;
                string query_insert ="INSERT INTO FORNECEDOR(nome) VALUES($NOME)";
                db.prepare_v2(query_insert,query_insert.length,out stinsert);
                stinsert.bind_text(1,nome);
                if (stinsert.step()!=Sqlite.DONE)
                {
                    var dle = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    dle.run();dle.destroy();
                    return;
                }
                db.exec("select LAST_INSERT_ROWID() FROM FORNECEDOR",(tam,valor,cnomes)=> {
                    this.codigo.set_text(valor[0]);
                    return(Sqlite.OK);
                });
                this.nome.set_text(nome);
            }
            atualizar();
        }
        dlg.destroy();
    }

    private void atualizar()
    {
        Gtk.ListStore md = new Gtk.ListStore(2,typeof(string),typeof(string));
        TreeIter it = TreeIter();
        db.exec("SELECT codigo,nome FROM fornecedor ORDER BY nome",(tam,valores,cnomes)=>{
                md.append(out it);
                md.set(it,0,valores[0]);
                md.set(it,1,valores[1]);
                return(Sqlite.OK);
        });
        lista.set_model(md);
    }

   private void adiciona_internet()
    {
        if (codigo.get_text()!="")
        {
            var dlg=new DLGTelefone();
            dlg.set_title("Adicionar endereço de internet para %s".printf(nome.get_text()));
            if (dlg.run()==1)
            {
               int quantidade=0;
               db.exec("SELECT count(descricao) FROM internet WHERE codigo_fornecedor='%s' AND descricao='%s'".printf(codigo.get_text(),dlg.descricao.get_text()),(tam,valores,nomes)=>{quantidade=int.parse(valores[0]);return(Sqlite.DONE);});
               if (quantidade<=0)
               {
                   Statement stm;
                   string query = "INSERT INTO internet(codigo_fornecedor,descricao,valor) VALUES($CODIGO,$DESCRICAO,$VALOR)";
                   db.prepare_v2(query,query.length,out stm);
                   stm.bind_text(1,codigo.get_text());
                   stm.bind_text(2,dlg.descricao.get_text());
                   stm.bind_text(3,dlg.valor.get_text());
                   if(stm.step()!=Sqlite.DONE)
                   {
                    var dle = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    dle.run();dle.destroy();
                   }  else atualiza_lista_internet();

            }
        }
           dlg.destroy();
      }
    }
//----
    private void adiciona_telefone()
    {
        if (codigo.get_text()!="")
        {
            var dlg=new DLGTelefone();
            dlg.set_title("Adicionar telefone para %s".printf(nome.get_text()));
            if (dlg.run()==1)
            {
               int quantidade=0;
               db.exec("SELECT count(descricao) FROM telefone WHERE codigo_fornecedor='%s' AND descricao='%s'".printf(codigo.get_text(),dlg.descricao.get_text()),(tam,valores,nomes)=>{quantidade=int.parse(valores[0]);return(Sqlite.DONE);});
               if (quantidade<=0)
               {
                   Statement stm;
                   string query = "INSERT INTO telefone(codigo_fornecedor,descricao,valor) VALUES($CODIGO,$DESCRICAO,$VALOR)";
                   db.prepare_v2(query,query.length,out stm);
                   stm.bind_text(1,codigo.get_text());
                   stm.bind_text(2,dlg.descricao.get_text());
                   stm.bind_text(3,dlg.valor.get_text());
                   if(stm.step()!=Sqlite.DONE)
                   {
                    var dle = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    dle.run();dle.destroy();
                   }  else atualiza_lista_telefones();
            }
          }
            dlg.destroy();
        }
    }

    private void adiciona_endereco()
    {
        if (codigo.get_text()!="")
        {
            var dlg = new DLGEndereco();
            dlg.set_title("Novo endereço para %s".printf(nome.get_text()));
            int quantidade = 0;
            if (dlg.run()==1)
            {
                db.exec("SELECT count(descricao) FROM enderecos WHERE codigo_fornecedor='%s' AND descricao='%s'".printf(codigo.get_text(),dlg.descricao.get_text()),(tamanho,valores,nomes)=>
                {
                    quantidade=int.parse(valores[0]);
                    return(Sqlite.DONE);
                });
                if (quantidade>0)
                {
                    var dl = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Já existe endereço com esta descrição");
                    dl.run();dl.destroy();
                } else
                {
                    Statement stm;
                    string query= "INSERT INTO endereco(codigo_fornecedor,descricao,rua,numero,bairro,cidade,cep,estado) VALUES($CODIGO_FORNECEDOR,$RUA,$DESCRICAO,$NUMERO,$BAIRRO,$CIDADE,$CEP,$ESTADO);";
                    db.prepare_v2(query,query.length,out stm);
                    stm.bind_text(1,codigo.get_text());
                    stm.bind_text(2,dlg.descricao.get_text());
                    stm.bind_text(3,dlg.rua.get_text());
                    stm.bind_text(4,dlg.numero.get_text());
                    stm.bind_text(5,dlg.bairro.get_text());
                    stm.bind_text(6,dlg.cidade.get_text());
                    stm.bind_text(7,dlg.cep.get_text());
                    stm.bind_text(8,dlg.estado.get_text());
                    if (stm.step()!=Sqlite.DONE)
                    {
                        var dl = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Erro inserindo dados do endereço");
                        stderr.printf(db.errmsg());
                        dl.run();dl.destroy();
                    }
                }
            }
            dlg.destroy();
            atualiza_lista_enderecos();
        }
    }

    private void apaga_endereco()
    {
        Gtk.TreeIter it = Gtk.TreeIter();
        var model = lista_endereco.get_model();
        var resultado = this.lista_endereco.get_selection().get_selected(out model,out it);
        if (resultado) {
            GLib.Value codigo;
            model.get_value(it,0,out codigo);
            var dlg = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.INFO,ButtonsType.YES_NO,"Apagar o endereço selecionado?");
            int ret = dlg.run();dlg.destroy();
            if (ret==ResponseType.YES)
            {
               db.exec("DELETE FROM endereco WHERE codigo='%s'".printf((string)codigo));
               atualiza_lista_enderecos();
            }
        }
    }

    private void apaga_telefone()
    {
        Gtk.TreeIter it = Gtk.TreeIter();
        var model = lista_telefone.get_model();
        var resultado = this.lista_telefone.get_selection().get_selected(out model,out it);
        if (resultado) {
            GLib.Value codigo;
            GLib.Value descricao;
            model.get_value(it,0,out codigo);
            model.get_value(it,1,out descricao);
            var dlg = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.INFO,ButtonsType.YES_NO,"Apagar o telefone %s de %s?".printf((string)descricao,nome.get_text()));
            int ret = dlg.run();dlg.destroy();
            if (ret==ResponseType.YES)
            {
               db.exec("DELETE FROM telefone WHERE codigo='%s'".printf((string)codigo));
               atualiza_lista_telefones();
            }
        }
    }
    private void apaga_internet()
    {
        Gtk.TreeIter it = Gtk.TreeIter();
        var model = lista_internet.get_model();
        var resultado = this.lista_internet.get_selection().get_selected(out model,out it);
        if (resultado) {
            GLib.Value codigo;
            GLib.Value descricao;
            model.get_value(it,0,out codigo);
            model.get_value(it,1,out descricao);
            var dlg = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.INFO,ButtonsType.YES_NO,"Apagar o endereço de internet:\n %s de %s?".printf((string)descricao,nome.get_text()));
            int ret = dlg.run();dlg.destroy();
            if (ret==ResponseType.YES)
            {
               db.exec("DELETE FROM internet WHERE codigo='%s'".printf((string)codigo));
               atualiza_lista_internet();
            }
        }
    }

    private void edita_endereco()
    {
        Gtk.TreeIter it = Gtk.TreeIter();
        var md = lista_endereco.get_model();
        var resultado = lista_endereco.get_selection().get_selected(out md,out it);
        if (resultado)
        {
            var dlg = new DLGEndereco();
            GLib.Value codigo;
            md.get_value(it,0,out codigo);
            db.exec("SELECT descricao,rua,numero,bairro,cidade,cep,estado FROM endereco WHERE codigo='%s'".printf((string)codigo),(tam,valor,nome)=>{
                dlg.descricao.set_text(valor[0]);
                dlg.rua.set_text(valor[1]);
                dlg.numero.set_text(valor[2]);
                dlg.bairro.set_text(valor[3]);
                dlg.cidade.set_text(valor[4]);
                dlg.cep.set_text(valor[5]);
                dlg.estado.set_text(valor[6]);
                return(Sqlite.DONE);
            });
            if (dlg.run()==1)
            {
                string query = "UPDATE endereco SET descricao=$DESCRICAO,rua=$RUA,numero=$NUMERO,bairro=$BAIRRO,cidade=$CIDADE,cep=$CEP,estado=$ESTADO WHERE codigo=$CODIGO";
                Statement stm;
                db.prepare_v2(query,query.length,out stm);
                stm.bind_text(1,dlg.descricao.get_text());
                stm.bind_text(2,dlg.rua.get_text());
                stm.bind_text(3,dlg.numero.get_text());
                stm.bind_text(4,dlg.bairro.get_text());
                stm.bind_text(5,dlg.cidade.get_text());
                stm.bind_text(6,dlg.cep.get_text());
                stm.bind_text(7,dlg.estado.get_text());
                stm.bind_text(8,(string)codigo);
                if (stm.step()!=Sqlite.DONE)
                {
                    var dl = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    stderr.printf(db.errmsg());
                    dl.run();dl.destroy();
                }
            }
            dlg.destroy();
            atualiza_lista_enderecos();
        }
    }

    private void edita_telefone()
    {
        Gtk.TreeIter it = Gtk.TreeIter();
        Gtk.TreeModel md = lista_telefone.get_model();
        bool resultado = lista_telefone.get_selection().get_selected(out md,out it);
        if (resultado)
        {
            var dlg = new DLGTelefone();
            GLib.Value codigo;
            GLib.Value descricao;
            GLib.Value valor;
            md.get_value(it,0,out codigo);
            md.get_value(it,1,out descricao);
            md.get_value(it,2,out valor);
            dlg.descricao.set_text((string)descricao);
            dlg.valor.set_text((string)valor);
            if(dlg.run()==1)
            {
                Statement stm;
                string query =" UPDATE telefone SET descricao=$DESCRICAO,valor=$VALOR WHERE codigo=$CODIGO";
                db.prepare_v2(query,query.length,out stm);
                stm.bind_text(1,dlg.descricao.get_text());
                stm.bind_text(2,dlg.valor.get_text());
                stm.bind_text(3,(string)codigo);
                if (stm.step()!=Sqlite.DONE)
                {
                    var dl = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    stderr.printf(db.errmsg());
                    dl.run();dl.destroy();
                }
            }
            dlg.destroy();
            atualiza_lista_telefones();
       }
   }


   private void edita_internet()
    {
        Gtk.TreeIter it = Gtk.TreeIter();
        Gtk.TreeModel md = lista_internet.get_model();
        bool resultado = lista_internet.get_selection().get_selected(out md,out it);
        if (resultado)
        {
            var dlg = new DLGTelefone();
            GLib.Value codigo;
            GLib.Value descricao;
            GLib.Value valor;
            md.get_value(it,0,out codigo);
            md.get_value(it,1,out descricao);
            md.get_value(it,2,out valor);
            dlg.descricao.set_text((string)descricao);
            dlg.valor.set_text((string)valor);
            if(dlg.run()==1)
            {
                Statement stm;
                string query =" UPDATE internet SET descricao=$DESCRICAO,valor=$VALOR WHERE codigo=$CODIGO";
                db.prepare_v2(query,query.length,out stm);
                stm.bind_text(1,dlg.descricao.get_text());
                stm.bind_text(2,dlg.valor.get_text());
                stm.bind_text(3,(string)codigo);
                if (stm.step()!=Sqlite.DONE)
                {
                    var dl = new MessageDialog((Window)this.get_toplevel(),DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    stderr.printf(db.errmsg());
                    dl.run();dl.destroy();
                }
            }
            dlg.destroy();
            atualiza_lista_internet();
       }
   }
    private void atualiza_lista_enderecos()
    {
        var md = new Gtk.ListStore(3,typeof(string),typeof(string),typeof(string));
        Gtk.TreeIter it = Gtk.TreeIter();
        db.exec("SELECT codigo,descricao,rua,numero,bairro,cidade,cep,estado FROM endereco WHERE codigo_fornecedor='%s'".printf(codigo.get_text()),(tam,valores,nomes)=>
        {
            md.append(out it);
            string campo = "Rua: %s numero: %s\nBairro:%s Cidade%s\nCEP:%s Estado:%s".printf(valores[2],valores[3],valores[4],valores[5],valores[6],valores[7]);
            md.set(it,0,valores[0]);
            md.set(it,1,valores[1]);
            md.set(it,2,campo);
            return(Sqlite.OK);
        });
        lista_endereco.set_model(md);
    }

    private void atualiza_lista_telefones()
    {
        var md = new Gtk.ListStore(3,typeof(string),typeof(string),typeof(string));
        Gtk.TreeIter it = Gtk.TreeIter();
        db.exec("SELECT codigo,descricao,valor FROM telefone WHERE codigo_fornecedor='%s'".printf(codigo.get_text()),(tam,valores,nomes)=>
        {
            md.append(out it);
            md.set(it,0,valores[0]);
            md.set(it,1,valores[1]);
            md.set(it,2,valores[2]);
            return(Sqlite.OK);
        });
        lista_telefone.set_model(md);
    }

    private void atualiza_lista_internet()
    {
        var md = new Gtk.ListStore(3,typeof(string),typeof(string),typeof(string));
        Gtk.TreeIter it = Gtk.TreeIter();
        db.exec("SELECT codigo,descricao,valor FROM internet WHERE codigo_fornecedor='%s'".printf(codigo.get_text()),(tam,valores,nomes)=>
        {
            md.append(out it);
            md.set(it,0,valores[0]);
            md.set(it,1,valores[1]);
            md.set(it,2,valores[2]);
            return(Sqlite.OK);
        });
        lista_internet.set_model(md);
    }
}


//frames do endereço
// lista e botões para adicionar e apagar.
class TabEndereco:Bin {
    public TreeView lista = new TreeView();
    public ToolButton add_button;
    public ToolButton del_button;
    string tipo;
    public TabEndereco(string tipo)
    {
        var scw = new ScrolledWindow(null,null);
        scw.add(lista);
        var wbase = new Box(Orientation.VERTICAL,0);
        var tb = new Toolbar();
        var bt = new ToolButton(new Image.from_icon_name("gtk-new",IconSize.LARGE_TOOLBAR),"");
        this.add_button= bt;
        var bt2 = new ToolButton(new Image.from_icon_name("gtk-delete",IconSize.LARGE_TOOLBAR),"");
        this.del_button=bt2;
        lista.insert_column_with_attributes (-1, "Código",new Gtk.CellRendererText (), "text",0);
        lista.insert_column_with_attributes (-1, "Descrição",new Gtk.CellRendererText (), "text",1);
        lista.insert_column_with_attributes (-1, "",new Gtk.CellRendererText (), "text",2);
        lista.get_column(0).set_visible(false);
        lista.set_headers_visible(false);
        tb.add(bt);
        tb.add(bt2);
        wbase.pack_start(scw,true,true);
        wbase.pack_end(tb,false,true);
        add(wbase);
        this.tipo = tipo;
        this.show_all();
    }

    public Label create_label()
    {
        var lb = new Label(this.tipo);
        return(lb);
    }
}
