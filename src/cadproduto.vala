using Gtk,Sqlite;

public class CadProduto:Window
{
    Entry codigo = new Entry();
    Entry descricao = new Entry();
    TreeView produtos = new TreeView();
    TreeView fornecedores = new TreeView();
    public CadProduto(string app_path,string cat_codigo)
    {
        set_default_size(640,480);
        var wbase= new Box(Orientation.VERTICAL,0);
        set_title("Catalogo código %s".printf(cat_codigo));
        //
        codigo.set_text(cat_codigo);
        var fr = new Frame("Catalogo");
        fr.add(descricao);
        wbase.pack_start(fr,false,true);
        descricao.set_text(app_path);
        //
        //Barra de ferramentas 1
        var tb = new Toolbar();
        var bt1 =new ToolButton(new Image.from_icon_name("gtk-new",IconSize.LARGE_TOOLBAR),"");tb.add(bt1);
        var bt2 =new ToolButton(new Image.from_icon_name("gtk-delete",IconSize.LARGE_TOOLBAR),"");tb.add(bt2);
        wbase.pack_start(tb,false,false);
        //
        var fr2= new Frame("Produtos no catogo");
        var scw = new ScrolledWindow(null,null);
        scw.add(produtos);
        fr2.add(scw);
        wbase.pack_start(fr2,true,true);
        //
        var fr3= new Frame("Fornecedores:");
        scw = new ScrolledWindow(null,null);
        scw.add(fornecedores);

        fr3.add(scw);
        wbase.pack_start(fr3,true,true);
        //Barra de ferramentas 2
        tb = new Toolbar();
        bt1 =new ToolButton(new Image.from_icon_name("gtk-new",IconSize.LARGE_TOOLBAR),"");tb.add(bt1);
        bt2 =new ToolButton(new Image.from_icon_name("gtk-delete",IconSize.LARGE_TOOLBAR),"");tb.add(bt2);
        wbase.pack_start(tb,false,false);
        //
        add(wbase);

    }
}

public class DlgProduto:Dialog {
    Entry descricao = new Entry();
    Entry preco = new Entry();
    TextView obs = new TextView();
    public DlgProduto()
    {
        set_title("Produto");
        set_default_size(400,400);
        var wbase = get_content_area() as Box;
        var fr = new Frame("Descrição:");
        var fr1 = new Frame("Preço:");
        var fr2 = new Frame("Observações:");
        wbase.pack_start(fr,false,true);
        wbase.pack_start(fr1,false,true);
        wbase.pack_start(fr2,false,true);
        fr.add(descricao);
        fr1.add(preco);
        //
        var scw=new ScrolledWindow(null,null);
        scw.set_policy(PolicyType.AUTOMATIC,PolicyType.AUTOMATIC);
        fr2.add(scw);
        scw.add(obs);
        add_buttons("gtk-ok",1,"gtk-cancel",0);
    }
}
