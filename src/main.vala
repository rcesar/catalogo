/*
 * main.vala
 *
 * Copyright 2021 Rodrigo  <rcesarh27@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gtk,Sqlite;

Database db;
extern string abs_path(string path);

public class MainWindow:Window
{
    private TreeView lista;
    private Label status;
    private string app_path;
    private Notebook desktop = new Notebook();
    public MainWindow(string pt)
    {
        app_path=pt;
        set_default_size(800,600);
        set_title("Catalogos");
        var wbase = new Box(Orientation.VERTICAL,0);
        //barra de ferramentas
        Gdk.Pixbuf image = null;
        try {
            image=new Gdk.Pixbuf.from_file(pt+"Imagens/empresa.png");
        }catch(Error e)  { stderr.printf("Erro abrindo arquivo do icone do cadastro");}
        var tb = new Toolbar();
        var bt1 = new ToolButton(new Image.from_icon_name("gtk-quit",IconSize.LARGE_TOOLBAR),"");tb.add(bt1);
        bt1.clicked.connect(main_quit);
        tb.add(new SeparatorToolItem());
        var bt2 =new ToolButton(new Image.from_icon_name("gtk-new",IconSize.LARGE_TOOLBAR),"");tb.add(bt2);
        bt2.clicked.connect(add_catalogo);
        var bt3 =new ToolButton(new Image.from_icon_name("gtk-delete",IconSize.LARGE_TOOLBAR),"");tb.add(bt3);
        wbase.pack_start(tb,false,true,0);
        // Lista de catalogos
        var fr = new Frame("Catalogos:");
        lista = new TreeView();
        lista.insert_column_with_attributes (-1, "",new Gtk.CellRendererText (), "text",0);
        lista.insert_column_with_attributes (-1, "Descrição",new Gtk.CellRendererText (), "text",1);
        lista.row_activated.connect(this.abre_catalogo);
        lista.get_column(0).set_visible(false);
        var scw = new ScrolledWindow(null,null);
        scw.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        scw.add(lista);
        fr.add(scw);
        wbase.pack_start(fr,true,true);
        //Barra de estatus
        this.status = new Label("");
        db.exec("SELECT sqlite_version()",(tam,valor,cnomes) =>
            {
                string msg="";
                msg="Banco de dados:"+Path.build_filename(pt,"db","catalogos.db")+"\t\t"+"Usando sqlite versão:"+valor[0];
                this.status.set_label(msg);
                return(0);
        });
        wbase.pack_end(this.status,false,true);
        //
        var fr1 = new Frame("");
        fr1.add(wbase);
        desktop.append_page(fr1,new Label("Catalogos"));
        desktop.append_page(new CadFornecedor(), new Label("Cadastro de fornecedores"));
        add(desktop);
        this.atualizar();
    }
    private void add_catalogo() {
        CadCatalogo dlg = new CadCatalogo();
        if (dlg.run()==1)
        {
            Statement stm;
            var query="SELECT count(codigo) FROM catalogo WHERE descricao LIKE $descricao";
            db.prepare_v2(query,query.length,out stm);
            stm.bind_text(1,dlg.descricao.get_text());//os parametros começam em 1 e não em 0.
            int ret = stm.step();
            int contador=stm.column_int(0);
            if((contador<=0) && (ret==Sqlite.ROW))
            {
                Statement stm2;
                query="INSERT INTO catalogo(descricao,obs) VALUES($DESCRICAO,$OBS)";
                db.prepare_v2(query,query.length,out stm2);
                stm2.bind_text(1,dlg.descricao.get_text());
                stm2.bind_text(2,(string)dlg.obs.buffer.text.data);
                int ret2=stm2.step();
                if (ret2!=Sqlite.DONE) {
                    var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    dl.run();dl.destroy();
                }
            } else {
                    var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Já existe este catalogo");
                    dl.run();dl.destroy();
            }
        }
        dlg.destroy();
    }

    private void atualizar()
    {
        var md = new Gtk.ListStore(2,typeof(string),typeof(string));
        TreeIter it = TreeIter();
        db.exec("SELECT codigo,descricao FROM catalogo ORDER BY descricao",(tam,valores,nomes)=> {
                md.append(out it);
                md.set(it,0,valores[0]);
                md.set(it,1,valores[1]);
                return(Sqlite.OK);
         });
        this.lista.set_model(md);
    }

    private void abre_catalogo()
    {
        var md=this.lista.get_model();
        var it = TreeIter();
        var resultado = this.lista.get_selection().get_selected(out md,out it);
        if (resultado) {
            GLib.Value codigo;
            md.get_value(it,0,out codigo);
            var jn = new CadProduto(this.app_path,(string)codigo);
            jn.show_all();
        }
    }
}


public static int main(string[] args)
{
    string app_path =abs_path(Path.get_dirname(abs_path(args[0])));//Vergonhosamente o vala não tem função para saber o path absoluto
    app_path=app_path[0:(app_path.length-3)];//App path é o diretorio onde o programa esta instalado, nome do executavel menos o bin
    int ret = Database.open(Path.build_filename(app_path,"db","catalogos.db"),out db);
    if (ret!=Sqlite.OK) {
        stderr.printf("Erro abrindo banco de dados\nErro: %s \nCódigo: %i\n",db.errmsg(),ret);
        return(1);
    }
    ret=db.exec("create table if not exists catalogo(codigo integer primary key,descricao char(40),obs text)");
    if (ret!=Sqlite.OK) { stderr.printf(db.errmsg());}
    ret=db.exec("create table if not exists fornecedor(codigo integer primary key,nome char(140),obs text)");
    ret=db.exec("create table if not exists telefone(codigo integer primary key,codigo_fornecedor integer, descricao char(100),valor char(200))");
    ret=db.exec("create table if not exists internet(codigo integer primary key,codigo_fornecedor integer, descricao char(100),valor char(200))");
    ret=db.exec("create table if not exists endereco(codigo integer primary key,descricao char(100),codigo_fornecedor integer,rua char(100),numero char(5),bairro char(60),cidade char(60),estado char(2),cep char(20))");
    if (ret!=Sqlite.OK) { stderr.printf(db.errmsg());}
    Gtk.init(ref args);
    Gdk.Pixbuf image = null;
    try {
        image=new Gdk.Pixbuf.from_file(app_path+"Imagens/icone.png");
    }catch(Error e)  { stderr.printf("Erro abrindo arquivo do icone do aplicativo");}
    MainWindow jn =  new MainWindow(app_path);
    jn.set_icon(image);
    jn.destroy.connect(Gtk.main_quit);
    jn.show_all();
    Gtk.main();
    return(0);
}
