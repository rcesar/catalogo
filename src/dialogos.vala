using Gtk;

//DLGNome
class DLGNome:Dialog {
    public Entry nome = new Entry();
    public DLGNome(string pad="")
    {
        set_title("Cadastro de fornecedor - nome");
        set_default_size(600,100);
        var fr = new Frame("Nome:");
        fr.add(nome);
        if (pad!=""){nome.set_text(pad);}
        var content = base.get_content_area ();
        content.add(fr);
        add_buttons("gtk-ok",1,"gtk-cancel",0);
    }

    public new int run()
    {
        this.show_all();
        return(base.run());
    }
}
//Dialogo para telefone e internet
class DLGTelefone:Dialog {

    public Entry descricao = new Entry();
    public Entry valor = new Entry();
    public DLGTelefone()
    {
        Box wbase = get_content_area();
        var fr = new Frame("Descrição:");fr.add(descricao);
        var fr1= new Frame("Valor:");fr1.add(valor);
        wbase.pack_start(fr,false,true);
        wbase.pack_start(fr1,false,true);
        add_buttons("gtk-ok",1,"gtk-cancel",0);
    }
    public new int run()
    {
        show_all();
        return(base.run());
    }
}
//-----------------------------------
class DLGEndereco:Dialog {
    public Entry descricao = new Entry();
    public Entry rua = new Entry();
    public Entry numero = new Entry();
    public Entry bairro = new Entry();
    public Entry cidade = new Entry();
    public Entry cep = new Entry();
    public Entry estado = new Entry();

    public DLGEndereco()
    {
        set_title("Endereço:");
        set_default_size(500,300);
        var fr = new Frame("Descrição:");fr.add(descricao);
        var fr1 = new Frame("Rua:");fr1.add(rua);
        fr1.set_size_request(300,-1);
        var fr2 = new Frame("Numero:");fr2.add(numero);
        fr2.set_size_request(20,20);
        numero.set_size_request(20,20);
        var fr3 = new Frame("Bairro:");fr3.add(bairro);
        var fr4 = new Frame("Cidade:");fr4.add(cidade);
        var fr5 = new Frame("CEP:");fr5.add(cep);
        var fr6 = new Frame("Estado:");fr6.add(estado);
        var wbase = get_content_area();
        wbase.pack_start(fr,false,true);
        Gtk.Box linha = new Gtk.Box(Orientation.HORIZONTAL,0);
        linha.pack_start(fr1,true,true);
        linha.pack_start(fr2,false,false);
        wbase.pack_start(linha,false,true);
        linha = new Gtk.Box(Orientation.HORIZONTAL,0);
        linha.pack_start(fr3,true,true);
        linha.pack_start(fr4,true,true);
        wbase.pack_start(linha,false,true);
        linha = new Gtk.Box(Orientation.HORIZONTAL,0);
        linha.pack_start(fr5,true,true);
        linha.pack_start(fr6,false,false);
        wbase.pack_start(linha,false,true);
        add_buttons("gtk-ok",1,"gtk-cancel",0);
    }

    public  new int run()
    {
        this.show_all();
        return(base.run());
    }
}

public class CadCatalogo:Dialog
{
    public Entry codigo = new Entry();
    public Entry descricao = new Entry();
    public TextView obs = new TextView();

    public CadCatalogo()
    {
            set_title("Cadastro de catalogo");
            set_default_size(400,500);
            var wbase = get_content_area() as Box;//new Box(Orientation.VERTICAL,0);
            var fr = new Frame("Descrição:");
            fr.add(descricao);
            var fr2 = new Frame("Categorias:");
            var scw = new ScrolledWindow(null,null);
            fr2.add(scw);
            scw.set_policy(PolicyType.AUTOMATIC,PolicyType.AUTOMATIC);
            scw.add(obs);
            wbase.pack_start(fr,false,true);
            wbase.pack_start(fr2,true,true);
            //add(wbase);
            add_buttons("gtk-ok",1,"gtk-cancel",0);
    }
    public new int run()
    {
        show_all();
        return base.run();//keyword base permite acessar a classe original
    }
}

